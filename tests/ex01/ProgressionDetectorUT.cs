using System;
using ex01;
using Xunit;

namespace tests.ex01;

public class ProgressionDetectorUT
{
	[Theory]
	[InlineData(new int[] { 1 })]
	[InlineData(new int[] { 4, 2, 3, 4, 5, 6 })]
	public void IsProgression_InputStartWithNumberNoGreaterThan4_Throws(int[] input)
	{
		// arrange
		var o = new ProgressionDetector();

		// act
		// assert
		var ex = Assert.Throws<ArgumentException>(() =>
			o.IsProgression(input)
		);

		Assert.Contains("count", ex.ParamName);
	}

	[Theory]
	[InlineData(new int[] { 5 })]
	[InlineData(new int[] { 5, 1, 2, 3, 4 })]
	public void IsProgression_InputWithElementsLessThanCount_Throws(int[] input)
	{
		// arrange
		var o = new ProgressionDetector();

		// act
		// assert
		var ex = Assert.Throws<ArgumentException>(() =>
			o.IsProgression(input)
		);

		Assert.Contains("elements", ex.ParamName);
	}

	[Theory]
	[InlineData(new int[] { 7, 6, 8, 10, 12, 14, 16, 18 })] // from def
	[InlineData(new int[] { 5, 1, 2, 3, 4, 5, 6 })]
	[InlineData(new int[] { 6, 1, 3, 5, 7, 9, 11, 12 })]
	public void IsProgression_InputIsProgression_ReturnsYes(int[] input)
	{
		// arrange
		var o = new ProgressionDetector();

		// act
		var res = o.IsProgression(input);

		// assert
		Assert.Equal("YES", res);
	}

	[Theory]
	[InlineData(new int[] { 5, 17, 19, 21, 23, 27 })] // from def
	[InlineData(new int[] { 5, 1, 2, 13, 4, 5, 6 })]
	[InlineData(new int[] { 5, 1, 2, 3, 4, 6 })]
	public void IsProgression_InputIsNotProgression_ReturnsNo(int[] input)
	{
		// arrange
		var o = new ProgressionDetector();

		// act
		var res = o.IsProgression(input);

		// assert
		Assert.Equal("NO", res);
	}
}