Solutions uses dotnet 6.
To execute the code, first build the sources and give the input as arguments.
> :bulb: > dotnet build


1. [exercise 1](./ex01/ex01.md)
	> :bulb: > dotnet run --project ex01/ 5 1 2 3 4 5 
2. [exercise 2](./ex02/ex02.md)
	> :bulb: > dotnet run --project ex02/ babbaac
	