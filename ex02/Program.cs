﻿// See https://aka.ms/new-console-template for more information
using ex02;

try
{
	var nr = new NotationReducer();
	var ret = nr.ReduceNotation(args.Length > 0 ? args[0] : "");
	Console.WriteLine(ret);
}
catch (Exception ex)
{
	Console.WriteLine(ex.Message);
}	
