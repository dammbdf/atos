﻿// See https://aka.ms/new-console-template for more information
using ex01;

try
{
	// get the args as int[]
	var input = args.Select(x => int.Parse(x)).ToArray();

	var pd = new ProgressionDetector();
	var ret = pd.IsProgression(input);
	Console.WriteLine(ret);
}
catch (Exception ex)
{
	Console.WriteLine(ex.Message);
}