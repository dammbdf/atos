using System;
using ex02;
using Xunit;

namespace tests.ex02;

public class NotationReducerUT
{
	[Theory]
	[InlineData(null)]
	[InlineData("")]
	[InlineData(" ")]
	[InlineData("	")]
	public void ReduceNotation_InputIsNullOrWhitespace_Throws(string input)
	{
		// arrange
		var o = new NotationReducer();

		// act
		// assert
		var ex = Assert.Throws<ArgumentNullException>(() =>
			o.ReduceNotation(input)
		);
	}

	[Theory]
	[InlineData("~")]
	[InlineData("!")]
	[InlineData(".")]
	[InlineData("3")]
	[InlineData("Z")]
	[InlineData("a2")]
	public void ReduceNotation_InputWithUnexpectedChars_Throws(string input)
	{
		// arrange
		var o = new NotationReducer();

		// act
		// assert
		var ex = Assert.Throws<ArgumentException>(() =>
			o.ReduceNotation(input)
		);
	}

	[Theory]
	[InlineData("aabbaadddc", "a4b2d3c1")] // from def
	[InlineData("bbbbaaaadddeee", "b4a4d3e3")] // from def
	[InlineData("aabbacac", "a4b2c2")] // from def ???
	[InlineData("aaa", "a3")]
	[InlineData("aaabb", "a3b2")]
	public void ReduceNotation_InputIsValid_ReturnsReducedNotation(string input, string expected)
	{
		// arrange
		var o = new NotationReducer();

		// act
		var actual = o.ReduceNotation(input);

		// assert
		Assert.Equal(expected, actual);
	}
}