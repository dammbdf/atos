namespace ex01;

public class ProgressionDetector
{
	public string IsProgression(int[] input)
	{
		Validate(input);

		return YesNoAnalisys(input) ? "YES" : "NO";  
	}

	void Validate(int[] input)
	{
		var count = input[0];

		if (count <= 4)
		{
			throw new ArgumentException("Sequence start is not greater than 4!", $"{nameof(input)}.count");
		}

		if (input.Length <= count)
		{
			throw new ArgumentException($"Sequence has less than {count} elements!", $"{nameof(input)}.elements");
		}
	}

	bool YesNoAnalisys(int[] input)
	{
		var count = input[0];
		var elements = new int[count];
		System.Array.Copy(input, 1, elements, 0, count);

		var diff = elements[1] - elements[0];
		var prev = elements[1];
		for (int i = 2; i < count; i++)
		{
			var current = elements[i];
			if ( current - prev == diff)
			{
				prev = current;
			}
			else
			{
				return false;
			}
		}

		return true;
	}

}