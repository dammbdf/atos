using System.Text.RegularExpressions;

namespace ex02;

public class NotationReducer
{
	public string ReduceNotation(string input)
	{
		Validate(input);

		return Reduce(input);
	}

	void Validate(string input)
	{
		if (string.IsNullOrWhiteSpace(input))
		{
			throw new ArgumentNullException(nameof(input), "No input!");
		}

		var rgx = new Regex(@"^[a-z]+$");
		if (!rgx.IsMatch(input))
		{
			throw new ArgumentException("Unexpected chars", nameof(input));
		}
	}

	string Reduce(string input)
	{
		var letterCounts = new Dictionary<char, int>();

		var letters = input.ToCharArray();
		foreach (var letter in letters)
		{
			if (letterCounts.ContainsKey(letter))
			{
				letterCounts[letter] += 1;
			}
			else
			{
				letterCounts.Add(letter, 1);
			}
		}

		return string.Join("", letterCounts.Select(x => $"{x.Key}{x.Value}"));
	}
}